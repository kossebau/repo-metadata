#!/usr/bin/python3
# Copyright 2016 Boudhayan Gupta <bgupta@kde.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. Neither the name of KDE e.V. (or its successor approved by the
#    membership of KDE e.V.) nor the names of its contributors may be used
#    to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import os
import yaml

try:
    import simplejson as json
except ImportError:
    import json

try:
    from scandir import scandir
except ImportError:
    from os import scandir

def getRepoName(metadir):
    metafile = os.path.join(metadir, "metadata.yaml")
    if not os.path.isfile(metafile):
        return None

    with open(metafile) as f:
        meta = yaml.load(f)

    if not meta["hasrepo"]:
        return None
    return meta["repopath"]

def indexByRepo(root, base):
    repomap = {}
    for entry in scandir(root):
        if entry.is_dir():
            reponame = getRepoName(entry.path)
            if reponame:
                repomap[reponame] = entry.path[len(base) + 1:]
            subdirmap = indexByRepo(os.path.join(root, entry.name), base)
            if subdirmap:
                repomap.update(subdirmap)
    return repomap

if __name__ == "__main__":

    base = os.path.abspath("projects")
    index = indexByRepo(base, base)

    with open("output/repoindex.json", "w") as f:
        json.dump(index, f)
