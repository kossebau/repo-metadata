#!/usr/bin/python3
import os
import sys
import yaml
import argparse

# Gather the command line arguments we need
parser = argparse.ArgumentParser(description='Migration utility for KDE Repository Metadata')
parser.add_argument('--metadata-path', help='Path to the metadata we are updating', required=True)
args = parser.parse_args()

# Make sure our configuration file exists
if not os.path.exists( args.metadata_path ):
    print("Unable to locate specified metadata location: %s".format(args.metadata_path))
    sys.exit(1)

# Start going over the location in question...
for currentPath, subdirectories, filesInFolder in os.walk( args.metadata_path, topdown=False, followlinks=False ):
    # Determine the location the repository will be at in Gitlab
    gitlabLocation = currentPath[len(args.metadata_path):]
    print("== Working on: " + currentPath)

    # Do we have a metadata.yaml file?
    if 'metadata.yaml' not in filesInFolder:
        # We're not interested then....
        continue

    # Now that we know we have something to work with....
    # Lets load the current metadata up
    metadataPath = os.path.join( currentPath, 'metadata.yaml' )
    metadataFile = open( metadataPath, 'r' )
    metadata = yaml.load( metadataFile )

    # Now we need to clean it up!
    # 'icon' serves no purpose
    del metadata['icon']
    # 'members' doesn't have a purpose either
    del metadata['members']
    # 'type' is also redundant now
    del metadata['type']

    # With the cleanup done, add the new 'identifier' item
    # As we have a handful of items in prefixes (like sysadmin/ and websites/) we replaces slashes with dashes for safety purposes
    if metadata['repopath'] is not None:
        metadata['identifier'] = metadata['repopath'].replace('/', '-')
    else:
        metadata['identifier'] = None

    # Finally, we transform the 'repopath' to match what it will be on Gitlab
    metadata['repopath'] = gitlabLocation

    # Now we can write it out!
    with open(metadataPath, 'w') as output:
        yaml.dump( metadata, output, default_flow_style=False)

# All done!
sys.exit(0)

