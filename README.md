This repository holds metadata about KDE's projects and their organisation.

# Managing Projects

All projects live as individual directories inside the `projects/` directory. Inside `projects/` are the top-level divisions, or components. Under each component (such as `projects/frameworks/`) are the modules. Under each module are the projects themselves.

Just to add to the confusion, a module can be a project, and hold information about a repository containing its code.

## 1. Creating Projects

Creating a new project is a simple task. Just follow these steps:

1. First, choose where you want the project to live. This could be `frameworks/`, or `kde/kdegraphics`. Inside the respective folder under `projects/`, create a directory for your project. For example, let's say you want to create a new image editor called `PictureShoppe` - you'd create a directory in `projects/kde/kdegraphics/pictureshoppe`.
2. Now, you'll need to define the metadata for your project. This is defined in a YAML file called `metadata.yaml` that lives in your project folder, so go ahead and create it. The structure of the YAML file is described in the next section.
3. Finally, you'll need to define the branches for which translations are generated. This isn't strictly necessary, as defaults are assigned automatically, but if you want to override the defaults, you'll have to create a JSON file called `i18n.json` inside the directory. The structure of this file is also described in the next section.
4. And that's it! Regenerate the XML and Gitolite configurations and you're good to go!

## 2. File Structures

The first file you'll deal with is the `metadata.yaml` file. Here's a sample `metadata.yaml`:

    description: The new screenshot capture utility, replaces KSnapshot
    hasrepo: true
    icon: null
    members:
    - displayname: Boudhayan Gupta
      username: bgupta
    name: Screenshot Capture Utility
    projectpath: kde/kdegraphics/spectacle
    repoactive: true
    repopath: spectacle
    type: project

First, the mandatory fields. The fields `name`, `description` and `icon` are to provide a name, description and icon for the project. The `members` field is a list of maintainers for the project. Every maintainers is defined by two attributes - `displayname`, which can be what you want, and `username`, which must match your KDE Identity username. `projectpath` contains the logical path (in KDE's organisation structure) to the project. Currently, multiple scripts use this information and we don't generate it from the actual on disk path, so this field is mandatory. `type` can either be `module` or `project`, but for the sake of the generated XML being correct, please always set it to `module` if it's a directory containing projects and always to `project` if it's a leaf.

If your project has a repo, you'll need to set `hasrepo` to `true`. If `hasrepo` is `true`, then you'll have to define two more fields - `repopath`, a path to the repository on disk, and `repoactive`, stating if the repo is active.

The next file you'll deal with is `i18n.json`. This file is much simpler to interpret. Here's an example:

    {
        "trunk": "none",
        "stable": "none",
        "stable_kf5": "Applications/15.12",
        "trunk_kf5": "master"
    }

This file contains a single JSON object, where the key is the i18n branch and the value is the corresponding branch in your Git repository. It couldn't be any simpler.

## 3. Moving Projects

Moving a project is as simple as moving your project directory to wherever you want, and adjusting `metadata.yaml` to reflect the new `projectpath` and `type`. Regenerate the XML and Gitolite config, and you're done.

# Managing I18N Branches

Internationalization is managed through two files. The `i18n.json` file is per repository and is described above, but there's another way to assign i18n branches to multiple repositories at once.

The file `config/i18n_defaults.json` contains default assignments for i18n branches. This is a JSON file, with key-value pairs where the key is a shell-style wildcard expression that will match a particular project path.

For example, the entry:

    "frameworks/*" : {
        "trunk" : "none",
        "stable" : "none",
        "trunk_kf5" : "master",
        "stable_kf5" : "none"
    }

matches any project under `frameworks/` and assigns the branches as above to it.

**Important:** The order of entries in this file is significant. The XML builder matches project paths from the top of the file to the bottom, and takes the entries from the first match it finds. Therefore, all entries in this file should be listed from more specific patterns at the top to more general entries at the bottom. Indeed, the last entry in the file matches all projects.

Any `i18n.json` file inside a project directory overrides the entries that have been assigned from the defaults file. It is recommended that the per-project `i18n.json` file only list the particular branches that differ from the default branches.
